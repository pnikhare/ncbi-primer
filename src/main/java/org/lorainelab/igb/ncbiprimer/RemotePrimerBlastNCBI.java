package org.lorainelab.igb.ncbiprimer;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import apollo.datamodel.StrandedFeatureSetI;
import apollo.datamodel.SequenceI;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Sends and retrieves a request to NCBI's Primer-BLAST CGI.
 *
 * @author elee
 *
 */
public class RemotePrimerBlastNCBI {

    private static final String ENCODING = "UTF-8";
    private static final String PRIMER_BLAST_URL = "https://www.ncbi.nlm.nih.gov/tools/primer-blast/primertool.cgi";
    private static final int SLEEP = 5000;
    private static final Logger logger = LoggerFactory.getLogger(RemotePrimerBlastNCBI.class);

    private NCBIPrimerBlastModel opts;
    public String errorMessage = "";

    /**
     * Constructor.
     *
     * @param opts - Primer-BLAST options
     */
    public RemotePrimerBlastNCBI(NCBIPrimerBlastModel opts) {
        this.opts = opts;
    }

    /**
     * Submits primer blast and returns a url to view it.
     *
     * @param cs
     * @param seq
     * @return
     * @throws Exception
     */
    public String runAnalysis(StrandedFeatureSetI cs, SequenceI seq) throws Exception {
        InputStream response = sendRequest(seq);
        InputStream copy = copyStream(response);
        String resultsUrl = getRedirectionUrl(copy);
        //logger.info("runAnalysis: got resultsURL {}",resultsUrl);
        if (response != null) {
            response.close();
        }
        if (copy != null) {
            copy.close();
        }
        return resultsUrl;
    }

    /**
     * A Loraine notes - Jan 2020: This method fails with with a 403 error code
     * coming from the server. Looks like the server has changed since this was
     * written. Now, hitting the URL launches a job on the NCBI site, and user
     * sees an intermediate page with a link that will lead to a results set
     * once the job is finished. The link contains a job identifier. It looks
     * like the way to get this App to work properly would be to do something
     * similar to ProtAnnot, where the search gets launched in a new thread and
     * the GUI updates once the remote job is complete. I would also recommend
     * investigating further using the testing code in Test Packages.
     *
     * @param seq
     * @return
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    private InputStream sendRequest(SequenceI seq) throws UnsupportedEncodingException, IOException {
        StringBuilder putBuf = new StringBuilder();
        processOptions(putBuf);
        putBuf.append("INPUT_SEQUENCE=");
        //putBuf.append(URLEncoder.encode(">" + seq.getName() + "\n", ENCODING));
        putBuf.append(URLEncoder.encode(seq.getResidues(), ENCODING));
      //  logger.info("sendRequest: blast URL options {}", putBuf);
        URL url = new URL(PRIMER_BLAST_URL);
        URLConnection conn = url.openConnection();
        conn.setDoOutput(true);
        try ( OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream())) {
            wr.write(putBuf.toString());
            wr.flush();
        }
//    apollo.util.IOUtil.informationDialog("Primer-BLAST request sent");
        return conn.getInputStream();
    }

    private InputStream copyStream(InputStream is) throws IOException {
        StringBuilder buf = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = br.readLine()) != null) {
            buf.append(line + "\n");
        }

        return new ByteArrayInputStream(buf.toString().getBytes());
    }

    /**
     * Not working. Maybe fix this?
     *
     * @param is
     * @return
     * @throws IOException
     */
    private String getRedirectionUrl(InputStream is) throws IOException {
        Document document = Jsoup.parse(is, null, "");
        if(document!= null) {
            Elements ncbiUrl = document.select("html head meta[http-equiv=refresh]");
            if (!ncbiUrl.isEmpty()) {
                return ncbiUrl.attr("CONTENT").split(";")[1].replace("URL=", "").trim();
            }
            Elements ulError = document.select("ul.msg");
            String errorMessage = "";
            if (!ulError.isEmpty()) {
                Elements li = ulError.select("li.error");
                for (int i = 0; i < li.size(); i++) {
                    errorMessage += i + ". " + li.get(i).select("p.error").text() + "\n";
                }
            }
            if(!errorMessage.isEmpty()) {
                return "Error:" + errorMessage;
            }
            Elements ulInfo = document.select("ul.msg");
            String infoMessage = "";
            if (!ulInfo.isEmpty()) {
                Elements li = ulInfo.select("li.info");
                for (int i = 0; i < li.size(); i++) {
                    infoMessage += i + ". " + li.get(i).select("p.info").text() + "\n";
                }
            }
            if(!infoMessage.isEmpty()) {
                return "Info:" + infoMessage;
            }
        }
        //BufferedReader br = new BufferedReader(new InputStreamReader(is));
        //String line;
        /*while ((line = br.readLine()) != null) {
            if (line.contains("META HTTP-EQUIV=Refresh")) {
                String[] tokens = line.split("\\s+");
                return tokens[3].substring(4, tokens[3].length() - 2);
            }
            if (line.contains("class=\"error\"")) {
                String[] tokens = line.split("error");
               return "Error: "+tokens[2].substring(2, tokens[2].length() - 23);

            }
            if (line.contains("<p class=\"info\">")) {
                String[] tokens = line.replaceAll("[^a-zA-Z0-9\\s+]", "").split("info");
                return "Info:" + tokens[1];
            }
        }*/
        return null;
    }

    private void processOptions(StringBuilder buf) throws UnsupportedEncodingException {
        buf.append("PRIMER5_START=" + convertOptionToString(opts.getPrimer5Start()) + "&");
        buf.append("PRIMER5_END=" + convertOptionToString(opts.getPrimer5End()) + "&");
        buf.append("PRIMER3_START=" + convertOptionToString(opts.getPrimer3Start()) + "&");
        buf.append("PRIMER3_END=" + convertOptionToString(opts.getPrimer3End()) + "&");
        buf.append("PRIMER_LEFT_INPUT=" + convertOptionToString(opts.getPrimerLeftInput()) + "&");
        buf.append("PRIMER_RIGHT_INPUT=" + convertOptionToString(opts.getPrimerRightInput()) + "&");
        buf.append("PRIMER_PRODUCT_MIN=" + convertOptionToString(opts.getPrimerProductMin()) + "&");
        buf.append("PRIMER_PRODUCT_MAX=" + convertOptionToString(opts.getPrimerProductMax()) + "&");
        buf.append("PRIMER_NUM_RETURN=" + convertOptionToString(opts.getPrimerNumReturn()) + "&");
        buf.append("PRIMER_MIN_TM=" + convertOptionToString(opts.getPrimerMinTm()) + "&");
        buf.append("PRIMER_OPT_TM=" + convertOptionToString(opts.getPrimerOptTm()) + "&");
        buf.append("PRIMER_MAX_TM=" + convertOptionToString(opts.getPrimerMaxTm()) + "&");
        buf.append("PRIMER_MAX_DIFF_TM=" + convertOptionToString(opts.getPrimerMaxDiffTm()) + "&");
        buf.append("SEARCH_SPECIFIC_PRIMER=" + (opts.isSearchSpecificPrimer() ? "on" : "off") + "&");
        buf.append("ORGANISM=" + convertOptionToString(opts.getOrganism()) + "&");
        buf.append("PRIMER_SPECIFICITY_DATABASE=" + convertOptionToString(opts.getPrimerSpecificityDatabase().toCGIParameter()) + "&");
        // buf.append("PRIMER_SPECIFICITY_DATABASE=refseq_mrna&"); 
        buf.append("ORGDICT=" + convertOptionToString(opts.getPrimerSpecificityDatabase().toOrgDict()) + "&");
        buf.append("TOTAL_PRIMER_SPECIFICITY_MISMATCH=" + convertOptionToString(opts.getTotalPrimerSpecificityMismatch()) + "&");
        buf.append("PRIMER_3END_SPECIFICITY_MISMATCH=" + convertOptionToString(opts.getPrimer3endSpecificityMismatch()) + "&");
        buf.append("MISMATCH_REGION_LENGTH=" + convertOptionToString(opts.getMismatchRegionLength()) + "&");
        buf.append("PRODUCT_SIZE_DEVIATION=" + convertOptionToString(opts.getProductSizeDeviation()) + "&");
        buf.append("SPLICE_SITE_OVERLAP_5END=" + convertOptionToString(opts.getExonJunctionMatchMin5()) + "&");
        buf.append("SPLICE_SITE_OVERLAP_3END=" + convertOptionToString(opts.getExonJunctionMatchMin3()) + "&");
        buf.append("SPLICE_SITE_OVERLAP_3END_MAX=" + convertOptionToString(opts.getExonJunctionMatchMax3()) + "&");
        buf.append("PRIMER_ON_SPLICE_SITE=" + convertOptionToString(opts.getExonJunctionSpan()) + "&");
        if (opts.isIntronInclusion()) {
            buf.append("SPAN_INTRON=on" + "&");
        }
        buf.append("MAX_INTRON_SIZE=" + convertOptionToString(opts.getIntronMaxLengthRange()) + "&");
        buf.append("MIN_INTRON_SIZE=" + convertOptionToString(opts.getIntronMinLengthRange()) + "&");
    }

    private String convertOptionToString(Object opt) throws UnsupportedEncodingException {
        if (opt == null) {
            return "";
        }
        return URLEncoder.encode(opt.toString(), ENCODING);
    }

}
